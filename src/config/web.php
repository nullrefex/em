<?php

$params = require(__DIR__ . '/params.php');
$modules = require(__DIR__ . '/modules.php');

$config = [
    'id' => 'app',
    'name' => 'EM-контроль',
    'language' => 'uk',
    'basePath' => dirname(__DIR__),
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'runtimePath' => dirname(dirname(__DIR__)) . '/runtime',
    'bootstrap' => [
        'log',
        'user' => 'app\components\Events',
        'dialog' => 'app\modules\dialog\Bootstrap',
        'project' => 'app\modules\project\Bootstrap',
        'tempo' => 'app\modules\tempo\Bootstrap',
    ],
    'components' => [
        'view' => [
            'theme' => [
                'baseUrl' => '@web',
                'pathMap' => [
                    '@vendor/nullref/yii2-full-admin/src/views' => '@app/modules/admin/views',
                ],
            ],
        ],
        'formatter' => [
            'class' => 'app\components\Formatter',
        ],
        'request' => [
            'cookieValidationKey' => 'RiAveGUdUACvWZppHVevMJRGd5Rij8uh',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\modules\user\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
                'admin' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
                'user' => [
                    'class' => 'nullref\core\components\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
                'rbac' => [
                    'class' => 'nullref\core\components\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'settings' => [
            'class' => 'pheme\settings\components\Settings',
            'cache' => false,
            'frontCache' => false,
        ],
        'authClientCollection' => [
            'class' => yii\authclient\Collection::className(),
            'clients' => [
                'vkontakte' => [
                    'class' => 'dektrium\user\clients\VKontakte',
                    'clientId' => $params['auth']['vkontakte']['clientId'],
                    'clientSecret' => $params['auth']['vkontakte']['clientSecret'],
                ],
                'facebook' => [
                    'class' => 'dektrium\user\clients\Facebook',
                    'clientId' => $params['auth']['facebook']['clientId'],
                    'clientSecret' => $params['auth']['facebook']['clientSecret'],
                ],
                'google' => [
                    'class' => 'dektrium\user\clients\Google',
                    'clientId' => $params['auth']['google']['clientId'],
                    'clientSecret' => $params['auth']['google']['clientSecret'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'modules' => $modules,
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['components']['mailer'] = [
        'class' => 'yii\swiftmailer\Mailer',
        'useFileTransport' => YII_ENV_DEV,
    ];
} else  {
    $config['components']['mailer'] = [
        'class' => 'yii\swiftmailer\Mailer',
        'viewPath' => '@app/modules/user/views/mail',
        'transport' => [
            'class' => 'Swift_SmtpTransport',
            'host' => 'smtp.gmail.com',
            'username' => $params['mailer']['username'],
            'password' => $params['mailer']['password'],
            'port' => '465',
            'encryption' => 'ssl',
            'plugins' => [
                [
                    'class' => 'Swift_Plugins_LoggerPlugin',
                    'constructArgs' => [new Swift_Plugins_Loggers_ArrayLogger],
                ],
            ]
        ],
        'useFileTransport' => false,
    ];
}

return $config;
