<?php

return array_merge(require(__DIR__ . '/installed_modules.php'), [
    'core' => [
        'class' => 'nullref\core\Module'
    ],
    'admin' => [
        'class' => 'app\modules\admin\Module',
        'controllerMap' => [
            'main' => [
                'class' => 'app\modules\admin\controllers\MainController',
            ]
        ],
        'components' => [
            'menuBuilder' => 'app\components\MenuBuilder',
        ],
    ],
    'user' => [
        'class' => 'app\modules\user\Module',
        'layout' => '@app/modules/admin/views/layouts/main',
        'controllerMap' => [
            'admin' => [
                'class' => 'app\modules\user\controllers\AdminController',
            ],
            'profile' => [
                'class' => 'app\modules\user\controllers\ProfileController',
            ],
            'recovery' => [
                'class' => 'app\modules\user\controllers\RecoveryController',
            ],
            'registration' => [
                'class' => 'app\modules\user\controllers\RegistrationController',
            ],
            'security' => [
                'class' => 'app\modules\user\controllers\SecurityController',
            ],
            'settings' => [
                'class' => 'app\modules\user\controllers\SettingsController',
            ],
        ],
    ],
    'project' => [
        'class' => 'app\modules\project\Module'
    ],
    'rbac' => [
        'class' => 'app\modules\rbac\Module',
        'layout' => '@nullref/fulladmin/views/layouts/main',
        'controllerMap' => [
            'access' => [
                'class' => 'app\modules\rbac\controllers\AccessController',
            ]
        ],
    ],
    'dialog' => [
        'class' => 'app\modules\dialog\Module',
        'controllerMap' => [
            'message' => [
                'class' => 'nullref\dialog\controllers\MessageController',
            ]
        ],
        'components' => [
            'userManager' => [
                'class' => 'app\modules\dialog\components\UserManager',
                'modelClass' => 'app\modules\user\models\User',
            ],
        ]
    ],
    'settings' => [
        'class' => 'app\modules\settings\Module'
    ],
    'tempo' => [
        'class' => 'app\modules\tempo\Module',
    ],
    'comments' => [
        'class' => 'rmrevin\yii\module\Comments\Module',
        'userIdentityClass' => 'app\modules\user\models\User',
        'useRbac' => true,
    ],
]);