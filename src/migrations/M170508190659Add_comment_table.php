<?php

namespace app\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170508190659Add_comment_table extends Migration
{
    use MigrationTrait;

    public function safeUp()
    {
        $this->createTable('{{%comment}}', [
            'id' => $this->primaryKey(),
            'entity' => $this->string(),
            'from' => $this->string(),
            'text' => $this->text(),
            'deleted' => $this->boolean()->notNull()->defaultValue(false),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->getTableOptions());

        $this->createIndex('idx_entity', '{{%comment}}', ['entity']);
        $this->createIndex('idx_created_by', '{{%comment}}', ['created_by']);
        $this->createIndex('idx_created_at', '{{%comment}}', ['created_at']);
    }

    public function safeDown()
    {
        $this->dropTable('{{%comment}}');
    }
}
