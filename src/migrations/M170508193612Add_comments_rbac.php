<?php

namespace app\migrations;

use \rmrevin\yii\module\Comments\Permission;
use \rmrevin\yii\module\Comments\rbac\ItsMyComment;

use yii\db\Migration;

class M170508193612Add_comments_rbac extends Migration
{
    public function safeUp()
    {
        $AuthManager = \Yii::$app->getAuthManager();
        $ItsMyCommentRule = new ItsMyComment();

        $AuthManager->add($ItsMyCommentRule);

        $AuthManager->add(new \yii\rbac\Permission([
            'name' => Permission::CREATE,
            'description' => 'Can create own comments',
        ]));
        $AuthManager->add(new \yii\rbac\Permission([
            'name' => Permission::UPDATE,
            'description' => 'Can update all comments',
        ]));
        $AuthManager->add(new \yii\rbac\Permission([
            'name' => Permission::UPDATE_OWN,
            'ruleName' => $ItsMyCommentRule->name,
            'description' => 'Can update own comments',
        ]));
        $AuthManager->add(new \yii\rbac\Permission([
            'name' => Permission::DELETE,
            'description' => 'Can delete all comments',
        ]));
        $AuthManager->add(new \yii\rbac\Permission([
            'name' => Permission::DELETE_OWN,
            'ruleName' => $ItsMyCommentRule->name,
            'description' => 'Can delete own comments',
        ]));
    }

    public function safeDown()
    {
    }
}
