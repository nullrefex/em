<?php

namespace app\modules\project;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * project module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    public $controllerAliases = [
        '@app/modules/project/controllers',
    ];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\project\controllers';

    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('project', 'Projects'),
            'url' => '/project/project',
            'icon' => 'cubes',
            'order' => 1,
            'roles' => ['project-manager', 'developer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }
}
