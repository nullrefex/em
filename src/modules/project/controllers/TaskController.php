<?php

namespace app\modules\project\controllers;

use app\modules\project\models\Project;
use app\modules\project\models\Task;
use app\modules\project\models\TaskSearch;
use app\modules\rbac\filters\AccessControl;
use nullref\core\interfaces\IAdminController;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller implements IAdminController
{
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param $project_id
     * @return string|\yii\web\Response
     */
    public function actionCreate($project_id)
    {
        $project = $this->findProject($project_id);

        $model = $project->createNewTask();

        /**
         * Fetch attributes from request params
         */
        foreach (array_keys($model->attributes) as $attribute) {
            $value = Yii::$app->request->get($attribute);
            if ($value !== null) {
                $model->$attribute = $value;
            }
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                return $this->asJson([
                    'model' => $model,
                ]);
            }
            return $this->redirect(Yii::$app->request->get('redirect_url') ?: ['/project/task/view', 'id' => $model->id]);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->asJson([
                    'model' => $model,
                ]);
            }
            return $this->render('create', [
                'model' => $model,
                'project' => $project,
            ]);
        }
    }

    /**
     * @param $id
     * @return Project|null
     * @throws NotFoundHttpException
     */
    protected function findProject($id)
    {
        if (($model = Project::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax) {
                return $this->asJson([
                    'model' => $model,
                ]);
            }
            return $this->redirect(Yii::$app->request->get('redirect_url') ?: ['/project/task/view', 'id' => $model->id]);
        } else {
            if (Yii::$app->request->isAjax) {
                return $this->asJson([
                    'model' => $model,
                ]);
            }
            return $this->render('update', [
                'model' => $model,
                'project' => $model->project,
            ]);
        }
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $model->delete();

        return $this->redirect(Yii::$app->request->get('redirect_url') ?: ['index']);
    }
}
