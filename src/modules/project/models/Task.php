<?php

namespace app\modules\project\models;

use app\modules\user\interfaces\IDestinationAssign;
use app\modules\user\models\User;
use app\modules\user\traits\AssignTrait;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%project_task}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $project_id
 * @property integer $parent_task_id
 * @property integer $number
 * @property string $code
 * @property string $description
 * @property integer $status_id
 * @property integer $created_by_id
 * @property integer $updated_by_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 *
 * @property Project $project
 * @property User[] $users
 * @property array $userIds
 */
class Task extends ActiveRecord implements IDestinationAssign
{
    use AssignTrait;

    public $assignUsers;
    public $assignClass;
    public $assignClassField;

    public function setAssignParams()
    {
        $this->assignClass = ProjectTaskHasUser::className();
        $this->assignClassField = 'task_id';
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project_task}}';
    }

    /**
     * @inheritdoc
     * @return TaskQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TaskQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'parent_task_id', 'number', 'status_id', 'created_by_id', 'updated_by_id', 'created_at', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['name', 'code'], 'string', 'max' => 255],
            [['assignUsers'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            'blame' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by_id',
                'updatedByAttribute' => 'updated_by_id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('project', 'ID'),
            'name' => Yii::t('project', 'Name'),
            'project_id' => Yii::t('project', 'Project'),
            'parent_task_id' => Yii::t('project', 'Parent Task'),
            'number' => Yii::t('project', 'Number'),
            'code' => Yii::t('project', 'Code'),
            'description' => Yii::t('project', 'Description'),
            'status_id' => Yii::t('project', 'Status'),
            'created_by_id' => Yii::t('project', 'Created By ID'),
            'updated_by_id' => Yii::t('project', 'Updated By ID'),
            'created_at' => Yii::t('project', 'Created At'),
            'updated_at' => Yii::t('project', 'Updated At'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $project = $this->project;
            $project->last_task_number++;
            $project->save(false, ['last_task_number']);
        }
        parent::afterSave($insert, $changedAttributes);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(ProjectTaskHasUser::tableName(), ['task_id' => 'id']);
    }

    public function getUserIds()
    {
        return ProjectTaskHasUser::find()
            ->select(['user_id'])
            ->where(['task_id' => $this->id])
            ->column();
    }
}
