<?php

namespace app\modules\project\models;

use app\modules\user\interfaces\IAssign;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "project_has_user".
 *
 * @property integer $project_id
 * @property integer $role
 * @property integer $user_id
 */
class ProjectHasUser extends ActiveRecord implements IAssign
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_has_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'role', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'project_id' => Yii::t('project', 'Project ID'),
            'role' => Yii::t('project', 'Role'),
            'user_id' => Yii::t('project', 'User ID'),
        ];
    }

    public static function getAssignUserIds($entityId)
    {
        return self::find()
            ->select(['user_id'])
            ->where(['project_id' => $entityId])
            ->column();
    }

    public static function getCurrentProject(self $self)
    {
        return Project::findOne([
            'id' => $self->project_id,
        ]);
    }
}
