<?php

namespace app\modules\project\models;

use app\modules\tempo\models\Time;
use app\modules\user\interfaces\IDestinationAssign;
use app\modules\user\models\User;
use app\modules\user\traits\AssignTrait;
use nullref\useful\traits\Mappable;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%project}}".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $last_task_number
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Status[] $statuses
 * @property Task[] $tasks
 * @property User[] $users
 * @property array $userIds
 */
class Project extends ActiveRecord implements IDestinationAssign
{
    use Mappable;
    use AssignTrait;

    public $assignUsers;
    public $assignClass;
    public $assignClassField;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%project}}';
    }

    /**
     * @inheritdoc
     * @return ProjectQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }

    public function setAssignParams()
    {
        $this->assignClass = ProjectHasUser::className();
        $this->assignClassField = 'project_id';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code'], 'required'],
            [['last_task_number', 'created_at', 'updated_at'], 'integer'],
            [['name', 'code'], 'string', 'max' => 255],
            [['assignUsers'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('project', 'ID'),
            'name' => Yii::t('project', 'Name'),
            'code' => Yii::t('project', 'Code'),
            'last_task_number' => Yii::t('project', 'Last Task Number'),
            'created_at' => Yii::t('project', 'Created At'),
            'updated_at' => Yii::t('project', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatuses()
    {
        return $this->hasMany(Status::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable(ProjectHasUser::tableName(), ['project_id' => 'id']);
    }

    public function getUserIds()
    {
        return ProjectHasUser::find()
            ->select(['user_id'])
            ->where(['project_id' => $this->id])
            ->column();
    }


    /**
     * @return Task
     */
    public function createNewTask()
    {
        $model = new Task();
        $model->project_id = $this->id;
        $model->number = $this->last_task_number + 1;
        $model->code = $this->code . '-' . $model->number;

        return $model;
    }

    /**
     * @return array
     */
    public function getStatusTaskMap()
    {
        //@todo memoize this method
        return ArrayHelper::map($this->tasks, 'id', function ($model) {
            return $model;
        }, 'status_id');
    }

    /**
     *
     */
    public function afterDelete()
    {
        Time::deleteAll(['task_id' => $this->id]);
        parent::afterDelete();
    }

}
