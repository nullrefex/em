<?php

namespace app\modules\project\migrations;

use app\modules\project\models\Project;
use app\modules\project\models\Status;
use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170315094105Create_project_tables extends Migration
{
    public $projectTable = '{{%project}}';
    public $projectHasUserTable = '{{%project_has_user}}';
    public $projectTaskTable = '{{%project_task}}';
    public $projectTaskTableHasUser = '{{%project_task_has_user}}';
    public $projectStatusTable = '{{%project_status}}';

    use MigrationTrait;

    public function up()
    {
        //Projects

        $this->createTable($this->projectTable, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'code' => $this->string(),
            'last_task_number' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->getTableOptions());

        $this->createTable($this->projectHasUserTable, [
            'project_id' => $this->integer(),
            'role' => $this->integer(),
            'user_id' => $this->integer(),
        ], $this->getTableOptions());


        //Tasks

        $this->createTable($this->projectTaskTable, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'project_id' => $this->integer(),
            'parent_task_id' => $this->integer(),
            'number' => $this->integer(),
            'code' => $this->string(), // project code + number
            'description' => $this->text(),
            'status_id' => $this->integer(),

            'created_by_id' => $this->integer(), // link to user
            'updated_by_id' => $this->integer(), // link to user
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], $this->getTableOptions());

        $this->createTable($this->projectTaskTableHasUser, [
            'task_id' => $this->integer(),
            'user_id' => $this->integer(),
        ], $this->getTableOptions());

        //Statuses
        $this->createTable($this->projectStatusTable, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(),
            'name' => $this->string(),
        ], $this->getTableOptions());


        $project = new Project(['attributes' => [
            'name' => 'Em',
            'code' => 'EM',
        ]]);

        $project->save();


        $statuses = ['Backlog', 'New', 'In progress', ' In test', 'Done'];

        foreach ($statuses as $status) {
            $model = new Status(['attributes' => ['name' => $status, 'project_id' => $project->id]]);
            $model->save();
        }


    }

    public function down()
    {
        $this->dropTable($this->projectStatusTable);
        $this->dropTable($this->projectTaskTableHasUser);
        $this->dropTable($this->projectTaskTable);
        $this->dropTable($this->projectHasUserTable);
        $this->dropTable($this->projectTable);
    }
}
