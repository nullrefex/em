<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\project\widgets;


use Codeception\Exception\ConfigurationException;
use yii\base\Widget;

class Task extends Widget
{
    public $task;

    public function init()
    {
        if ($this->task == null) {
            throw new ConfigurationException('Property `project` should be set');
        }
        parent::init();
    }

    public function run()
    {
        return $this->render('task', [
            'task' => $this->task,
        ]);
    }
}