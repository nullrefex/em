<?php

use app\modules\project\widgets\Task;
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\jui\JuiAsset;

/**
 * @var $this \yii\web\View
 * @var $project \app\modules\project\models\Project
 * @var $id string
 *
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
$statusTaskMap = $project->getStatusTaskMap();

JuiAsset::register($this);

$this->registerJs(<<<JS
var widget = jQuery('#$id');

widget.on('click', '.task-widget', function () {
    $(this).toggleClass('selected');
});

widget.find(".task-column").sortable({
    connectWith: '.task-column',
    opacity: 0.6,
    revert: true,
    helper: function (e, item) {
        if (!item.hasClass('selected')) {
            item.addClass('selected');
        }
        var elements = $('.selected').not('.ui-sortable-placeholder').clone();
        var helper = $('<ul/>');
        item.siblings('.selected').addClass('hidden');
        return helper.append(elements);
    },
    start: function (e, ui) {
        var elements = ui.item.siblings('.selected.hidden').not('.ui-sortable-placeholder');
        ui.item.data('items', elements);
    },
    receive: function (e, ui) {
        ui.item.before(ui.item.data('items'));
    },
    stop: function (e, ui) {
        ui.item.siblings('.selected').removeClass('hidden');
        $('.selected').removeClass('selected');
        updateTaskStatus(ui.item);
    }
});

function updateTaskStatus(item) {
    var taskId = item.data('id');
    var newStatusId = item.parent('.task-column').data('statusId');
    if (item.data('statusId') === newStatusId) {
        return;
    }
    item.css('opacity', 0.6);
    jQuery.ajax({
        method: 'post',
        url: '/project/task/update'.addUrlParam('id', taskId),
        data: {
            Task: {
                status_id: newStatusId
            }
        },
        success: function () {
            item.css('opacity', 1);
            item.data('statusId', newStatusId);
        }
    });
}

$(".task-column").disableSelection();

JS
);

?>

<div class="board-widget" id="<?= $id ?>">
    <table class="table table-striped">
        <tr>
            <?php foreach ($project->statuses as $status): ?>
                <th class="text-center">
                    <?= Html::encode($status->name) ?>
                    <span class="pull-right">
                        <?= Html::a(FA::i(FA::_PLUS), ['/project/task/create',
                            'project_id' => $project->id,
                            'status_id' => $status->id,
                            'redirect_url' => Url::current(),
                        ], ['class' => 'btn btn-primary btn-xs']) ?>
                </span>
                </th>
            <?php endforeach ?>
        </tr>
        <tr>
            <?php foreach ($project->statuses as $status): ?>
                <td class="task-column" data-status-id="<?= $status->id ?>">
                    <?php if (array_key_exists($status->id, $statusTaskMap)): ?>
                        <?php foreach ($statusTaskMap[$status->id] as $task): ?>
                            <?= Task::widget(['task' => $task]) ?>
                        <?php endforeach ?>
                    <?php endif ?>
                </td>
            <?php endforeach ?>
        </tr>
    </table>
</div>

