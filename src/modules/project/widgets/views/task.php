<?php
/**
 * @var $this \yii\web\View
 * @var $task \app\modules\project\models\Task
 *
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\helpers\Url;

?>
<div class="panel panel-default task-widget" data-id="<?= $task->id ?>" data-status-id="<?= $task->status_id ?>">
    <div class="panel-heading">
        <span>
            <?= FA::i(FA::_BARS) ?>
        </span>
        <?= Html::a($task->code, ['/project/task/view',
            'id' => $task->id,
        ]) ?>
        <?= Html::encode($task->name) ?>
        <span class="pull-right">
            <?= Html::a(FA::i(FA::_PENCIL), ['/project/task/update',
                'id' => $task->id,
                'redirect_url' => Url::current(),
            ], ['class' => 'btn btn-primary btn-xs']) ?>
        </span>
    </div>
    <div class="panel-body">
        <?= Html::encode(StringHelper::truncate($task->description, 100)) ?>
    </div>
    <div class="panel-footer">

    </div>
</div>
