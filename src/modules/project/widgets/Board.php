<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\project\widgets;


use Codeception\Exception\ConfigurationException;
use yii\base\Widget;

class Board extends Widget
{
    public $project;

    public function init()
    {
        if ($this->project == null) {
            throw new ConfigurationException('Property `project` should be set');
        }
        parent::init();
    }

    public function run()
    {
        return $this->render('board', [
            'project' => $this->project,
            'id' => $this->getId(),
        ]);
    }
}