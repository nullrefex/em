<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE\
 *
 *
 * @var $tasks \app\modules\project\models\Task[]
 */
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-lg-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-tasks fa-fw"></i> <?= Yii::t('project', 'Your tasks') ?>
        </div>
        <div class="panel-body">
            <div class="list-group">
                <?php if (count($tasks)): ?>
                    <?php foreach ($tasks as $task): ?>
                        <a href="<?= Url::to(['/project/task/view', 'id' => $task->id,]) ?>" class="list-group-item">
                            <i class="fa fa-circle fa-fw"></i>
                            <?= Html::encode($task->code) ?>
                            <?= Html::encode($task->name) ?>
                        </a>
                    <?php endforeach ?>
                <?php else: ?>
                    <h3><?= Yii::t('project', 'You don\'t have any tickets') ?></h3>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
