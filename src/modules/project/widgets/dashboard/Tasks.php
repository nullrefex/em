<?php

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */

namespace app\modules\project\widgets\dashboard;

use app\modules\user\models\User;
use Yii;
use yii\base\Widget;

class Tasks extends Widget
{
    public function run()
    {
        /** @var User $user */
        $user = Yii::$app->user->getIdentity();
        return $this->render('tasks', [
            'tasks' => $user->tasks,
        ]);
    }
}