<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 */


namespace app\modules\project\traits;


use app\modules\project\models\Task;
use yii\db\ActiveQuery;

/**
 * Trait ProjectUserTrait
 * @package app\modules\project\traits
 *
 * @method ActiveQuery hasMany($class, $link)
 *
 * @property $tasks Task[]
 *
 */
trait ProjectUserTrait
{
    /**
     *
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['id' => 'task_id'])
            ->viaTable('project_task_has_user', ['user_id' => 'id']);
    }

    /**
     *
     */
    public function getProjects()
    {

    }
}