<?php

use app\modules\project\widgets\Board;
use yii\bootstrap\Collapse;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\jui\Accordion;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Project */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('project', 'Projects'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-view">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('project', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('project', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('project', 'Statuses'), ['/project/status', 'project_id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('project', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('project', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <?= Collapse::widget(['items' => [
        [
            'label' => Yii::t('project', 'Details'),
            'content' => DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id',
                    'name',
                    'code',
                    'created_at:datetime',
                    'updated_at:datetime',
                ],
            ]),
        ],
        [
            'label' => Yii::t('project', 'Agile board'),
            'content' => Board::widget(['project' => $model]),
            'contentOptions' => ['class' => 'in'],
            'options' => [],
        ],
    ]]);
    ?>
    <br>
</div>
