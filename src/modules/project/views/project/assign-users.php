<?php

use app\modules\project\models\Project;
use app\modules\project\models\ProjectHasUser;
use app\modules\user\widgets\AssignUsers;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $id integer
 */

$this->title = Yii::t('user', 'Add users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="assign-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('user', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= AssignUsers::widget([
        'entityId' => $id,
        'assignClass' => ProjectHasUser::className(),
        'destinationClass' => Project::className()
    ]) ?>

</div>
