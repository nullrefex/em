<?php

use app\modules\project\models\Project;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\project\models\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('project', 'Projects');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="project-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('project', 'Create Project'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'code',
                'format' => 'raw',
                'value' => function (Project $model) {
                    return Html::a($model->code, ['/project/project/view', 'id' => $model->id]);
                }
            ],
            [
                'attribute' => 'name',
                'format' => 'raw',
                'value' => function (Project $model) {
                    return Html::a($model->name, ['/project/project/view', 'id' => $model->id]);
                }
            ],
            'created_at:datetime',
            'updated_at:datetime',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update} {delete} {addUsers}',
                'buttons' => [
                    'addUsers' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-user"></span>', ['assign-users',
                            'id' => $model->id,
                        ], [
                            'title' => Yii::t('user', 'Add users'),
                            'aria-label' => Yii::t('user', 'Add users'),
                            'data-method' => 'post',
                        ]);
                    }
                ],
            ],
        ],
    ]); ?>

</div>
