<?php
use rmrevin\yii\module\Comments\models\Comment;
use rmrevin\yii\module\Comments\widgets\CommentFormWidget;
use rmrevin\yii\module\Comments\widgets\CommentListWidget;

/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $model \app\modules\project\models\Task
 */
?>
<div>
    <?= CommentListWidget::widget([
        'entity' => 'task-' . $model->id,
        'viewFile' => '@app/modules/project/widgets/views/comment-list.php',
    ]) ?>
    <?= CommentFormWidget::widget([
        'Comment' => new Comment(),
        'entity' => 'task-' . $model->id,
        'viewFile' => '@app/modules/project/widgets/views/comment-form.php',
    ]) ?>
</div>

