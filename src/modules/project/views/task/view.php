<?php

use app\modules\project\models\ProjectTaskHasUser;
use app\modules\project\models\Task;
use app\modules\tempo\widgets\Table;
use app\modules\user\widgets\AssignUsers;
use yii\bootstrap\Collapse;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Task */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('project', 'Tasks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-view">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('project', 'Project'), ['/project/project/view', 'id' => $model->project_id], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('project', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('project', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('project', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>


    <?= Collapse::widget(['items' => [
        [
            'label' => Yii::t('project', 'Details'),
            'content' => DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'code',
                    'name',
                    'project_id:projectLink',
                    'parent_task_id',
                    'status_id:projectStatus',
                    'created_by_id:user',
                    'updated_by_id:user',
                    'created_at:datetime',
                    'updated_at:datetime',
                ],
            ]),
        ],
        [
            'label' => Yii::t('project', 'Description'),
            'content' => Html::encode($model->description),
            'contentOptions' => ['class' => 'in'],
            'options' => [],
        ],
        [
            'label' => Yii::t('user', 'Users'),
            'content' => AssignUsers::widget([
                'entityId' => $model->id,
                'assignClass' => ProjectTaskHasUser::className(),
                'destinationClass' => Task::className(),
            ]),
            'options' => [],
        ],
        [
            'label' => Yii::t('tempo', 'Time tracking'),
            'content' => Table::widget([
                'projectId' => false,
                'taskId' => $model->id,
                'addUrl' => ['/tempo/time/create',
                    'user_id' => Yii::$app->user->identity->getId(),
                    'task_id' => $model->id,
                    'redirect_url' => Url::current(),
                ],
            ]),
            'contentOptions' => ['class' => 'in'],
        ],
        [
            'label' => Yii::t('project', 'Comments'),
            'content' => $this->render('_comments', [
                'model' => $model,
            ]),
        ],
    ]]); ?>
    <br>
</div>
