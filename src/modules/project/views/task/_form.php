<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\project\models\Task */
/* @var $form yii\widgets\ActiveForm */
/* @var $project app\modules\project\models\Project */
?>

<div class="task-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'code')->textInput(['maxlength' => true, 'disabled' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'number')->textInput(['disabled' => true]) ?>
                </div>
            </div>

            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'parent_task_id')
                ->dropDownList(ArrayHelper::map($project->tasks, 'id', 'name'), ['prompt' => Yii::t('project', 'Task')]) ?>

            <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map($project->statuses, 'id', 'name')) ?>
        </div>
        <div class="col-md-12">
            <!-- @TODO add user input -->
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('project', 'Create') : Yii::t('project', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
