<?php


namespace app\modules\settings\models;


use app\modules\catalog\models\Category;
use app\modules\catalog\models\Product;
use app\modules\settings\components\Settings;
use app\modules\settings\traits\SettingTrait;
use Yii;
use yii\base\Model;

/**
 * Class GeneralSettings
 * @package app\modules\settings\models
 *
 * @property integer $appDialog
 */
class GeneralSettings extends Model
{
    use SettingTrait;

    const SECTION = 'general';

    const KEY_APP_DIALOG = 'appDialog';

    public $appDialog;

    public static function getKeys()
    {
        return [
            'appDialog' => [
                'name' => self::KEY_APP_DIALOG,
                'type' => 'string'
            ],
        ];
    }

    public static function getSerializedFields()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['appDialog'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'appDialog' => Yii::t('settings', 'App dialog'),
        ];
    }


}