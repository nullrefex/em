<?php

namespace app\modules\settings;

use app\components\RoleContainer;
use nullref\core\interfaces\IAdminModule;
use rmrevin\yii\fontawesome\FA;
use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule implements IAdminModule
{
    public static function getAdminMenu()
    {
        return [
            'label' => Yii::t('settings', 'Settings'),
            'icon' => FA::_COGS,
            'order' => 8,
            'items' => [
                'general' => [
                    'label' => Yii::t('settings', 'General settings'),
                    'url' => '/settings/general',
                    'icon' => FA::_COGS,
                ],
            ]
        ];
    }
}
