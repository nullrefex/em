<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\settings\traits;

use pheme\settings\components\Settings;
use Yii;

trait SettingTrait
{
    public function __construct($config = [])
    {
        $this->setValues();
        parent::__construct($config);
    }

    public function save()
    {
        /** @var Settings $settings */
        $settings = Yii::$app->settings;
        $values = self::getKeys();
        $serializedFields = self::getSerializedFields();
        foreach ($values as $key => $val) {
            $value = $this->{$val['name']};
            if (in_array($val['name'], $serializedFields)) {
                $value = serialize($value);
            }
            if ($value) {
                $settings->set(self::SECTION . '.' . $val['name'], $value, null, $val['type']);
            }

        }
    }

    public function setValues()
    {
        /** @var Settings $settings */
        $settings = Yii::$app->get('settings');
        $values = self::getKeys();
        $serializedFields = self::getSerializedFields();
        foreach ($values as $key => $val) {
            $value = $settings->get(self::SECTION . '.' . $val['name']);
            if (in_array($val['name'], $serializedFields)) {
                $value = unserialize($value);
            }
            if ($value != null) {
                $this->{$val['name']} = $value;
            }
        }
    }

    public static function getSerializedFields()
    {
        return [];
    }
}