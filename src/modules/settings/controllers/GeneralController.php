<?php


namespace app\modules\settings\controllers;


use app\modules\rbac\filters\AccessControl;
use app\modules\settings\models\GeneralSettings;
use nullref\core\interfaces\IAdminController;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;

class GeneralController extends Controller implements IAdminController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new GeneralSettings();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionChange()
    {
        $model = new GeneralSettings();

        if ($model->load(Yii::$app->request->post()) and $model->validate()) {
            $model->save();
            $this->redirect(['index']);
        }

        return $this->render('change', [
            'model' => $model,
        ]);
    }
}