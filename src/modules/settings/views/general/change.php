<?php
/**
 * @author    Yaroslav Velychko
 */


use app\modules\dialog\models\Dialog;
use app\modules\settings\models\GeneralSettings;
use yii\bootstrap\Html;
use yii\web\View;
use yii\widgets\ActiveForm;


/**
 * @var $model GeneralSettings
 * @var $this View
 */

$this->title = Yii::t('settings', 'General settings');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="general-settings">
    <div class="row">
        <div class="col-xs-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12">
            <?= Html::a(Yii::t('settings', 'Back to list'), ['/settings/general/'], ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-md-12">
            <?php $form = ActiveForm::begin(['id' => 'general-group-form']) ?>

            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $form->field($model, 'appDialog')->dropDownList(Dialog::getDropDownArray('id', 'title'),[
                                'prompt' => Yii::t('settings', 'Chose...')
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton(Yii::t('settings', 'Save'), ['class' => 'btn btn-success']) ?>
            </div>

            <?php $form = ActiveForm::end() ?>
        </div>
    </div>
</div>