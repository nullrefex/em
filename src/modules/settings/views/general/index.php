<?php
/**
 * @author    Yaroslav Velychko
 */

use app\modules\settings\models\GeneralSettings;
use yii\bootstrap\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $model GeneralSettings
 */

$this->title = Yii::t('settings', 'General settings');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="general-setting-index-page">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?= Html::a(Yii::t('settings', 'Change'), ['/settings/general/change'], ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <br>
    <table class="content-table table table-striped table-bordered">
        <thead>
        <tr>
            <th><?= Yii::t('settings', 'Name') ?></th>
            <th><?= Yii::t('settings', 'Value') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach (GeneralSettings::getKeys() as $key => $val): ?>
            <tr>
                <td><?= $model->getAttributeLabel($val['name']) ?></td>
                <?php if ($val['name'] == 'sites'): ?>
                    <td><?= $model->getSiteNames() ?></td>
                <?php elseif (is_array($model->{$val['name']})): ?>
                    <td>
                        <?php foreach ($model->{$val['name']} as $value): ?>
                            <?= $value ?>
                        <?php endforeach; ?>
                    </td>
                <?php else : ?>
                    <td><?= $model->{$val['name']} ?></td>
                <?php endif; ?>

            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

</div>