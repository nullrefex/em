<?php

namespace app\modules\settings\migrations;

use nullref\core\traits\MigrationTrait;
use yii\db\Migration;

class M170505125953Settings_init extends Migration
{
    use MigrationTrait;

    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'type' => $this->string(255)->notNull(),
            'section' => $this->string(255)->notNull(),
            'key' => $this->string(255)->notNull(),
            'value' => $this->text(),
            'active' => $this->boolean(),
            'created' => $this->dateTime(),
            'modified' => $this->dateTime(),
        ], $this->getTableOptions());

        $this->createIndex('settings_unique_key_section', '{{%settings}}', ['section', 'key'], true);

        return true;
    }

    public function safeDown()
    {
        $this->dropIndex('settings_unique_key_section', '{{%settings}}');
        $this->dropTable('{{%settings}}');

        return true;
    }
}
