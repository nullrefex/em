<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\admin;

use nullref\core\interfaces\IHasMigrateNamespace;
use nullref\fulladmin\Module as BaseModule;

class Module extends BaseModule implements IHasMigrateNamespace
{
    public $controllerAliases = [
        '@app/modules/admin/controllers',
        '@nullref/fulladmin/controllers',
    ];

    public function getMigrationNamespaces($defaults)
    {
        return [
            'app\modules\admin\migrations',
            'nullref\fulladmin\migrations',
            'nullref\fulladmin\modules\user\migrations',
        ];
    }


}