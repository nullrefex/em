<?php
/**
 * @author    Velychko Yaroslav
 */

namespace app\modules\dialog;

use nullref\core\interfaces\IAdminModule;
use nullref\dialog\Module as BaseModule;
use rmrevin\yii\fontawesome\FA;

class Module extends BaseModule implements IAdminModule
{
    public $controllerAliases = [
        '@app/modules/dialog/controllers',
        '@nullref/dialog/controllers',
    ];

    public function init()
    {
        parent::init();
    }

    public static function getAdminMenu()
    {
        return [
            'label' => \Yii::t('dialog', 'Chats'),
            'icon' => FA::_WECHAT,
            'order' => 4,
            'items' => [
                [
                    'label' => \Yii::t('dialog', 'Chats management'),
                    'icon' => FA::_WEIXIN,
                    'url' => '/dialog/dialog',
                    'roles' => ['user-manager'],
                ],
                [
                    'label' => \Yii::t('dialog', 'Chats'),
                    'icon' => FA::_WECHAT,
                    'url' => '/dialog/dialog/dialogs',
                    'roles' => ['user-manager'],
                ],
            ]
        ];
    }

}