<?php

namespace app\modules\dialog\controllers;

use app\modules\dialog\models\Dialog;
use app\modules\rbac\filters\AccessControl;
use app\modules\user\models\User;
use nullref\core\interfaces\IAdminController;
use nullref\dialog\models\Message;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * DialogController
 */
class DialogController extends Controller implements IAdminController
{
    public function behaviors()
    {
        return ArrayHelper::merge([
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ], parent::behaviors());
    }

    /**
     * Lists all Dialog models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Dialog::find()
                ->with(['userRelation', 'project']),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Dialog model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Dialog model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Dialog();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/dialog/dialog/view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Dialog model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/dialog/dialog/view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Dialog model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/dialog/dialog/index']);
    }

    /**
     * Finds the Dialog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Dialog the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Dialog::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionAssignUsers($id)
    {
        return $this->render('assign-users', [
            'id' => $id
        ]);
    }

    public function actionDialogs()
    {
        /** @var User $user */
        if ($user = Yii::$app->user->identity) {
            $dialogs = $user->allDialogs;

            return $this->render('dialogs', [
                'dialogs' => $dialogs
            ]);
        }
    }

    public function actionDialog($id)
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        $dialog = Dialog::findOne($id);

        return $this->render('dialog', [
            'user' => $user,
            'dialog' => $dialog,
        ]);
    }

    public function actionWriteTo($userId)
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        if ($user->id != $userId) {
            $dialog = Dialog::getDialogForTwo($user, $userId);
            if ($dialog) {
                return $this->redirect(['/dialog/dialog/dialog', 'id' => $dialog->id]);
            }
        }

        return $this->redirect(['/dialog/dialog/dialogs']);
    }

    public function actionCheckMessage($id, $lastId = null)
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            /** @var array $message */
            $message = Message::find()
                ->select(['id'])
                ->where(['dialog_id' => $id,])
                ->orderBy(['id' => SORT_DESC])
                ->asArray()
                ->one();
            if ($message['id'] != $lastId) {
                return true;
            }

            return false;
        }

        return $this->redirect(['index']);
    }
}
