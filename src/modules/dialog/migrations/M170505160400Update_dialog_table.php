<?php

namespace app\modules\dialog\migrations;

use yii\db\Migration;

class M170505160400Update_dialog_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%dialog}}', 'type', $this->integer());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%dialog}}', 'type', $this->string());

        return true;
    }

}