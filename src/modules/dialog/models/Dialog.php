<?php
namespace app\modules\dialog\models;

/**
 * @author    Yaroslav Velychko
 */

use app\modules\project\models\Project;
use app\modules\user\interfaces\IDestinationAssign;
use app\modules\user\models\User;
use app\modules\user\traits\AssignTrait;
use nullref\dialog\models\Dialog as BaseDialog;
use nullref\useful\DropDownTrait;
use Yii;

/**
 * @property string $title
 * @property integer $project_id
 *
 * @project Project $project
 * @project User $user
 */
class Dialog extends BaseDialog implements IDestinationAssign
{
    use DropDownTrait;
    use AssignTrait;

    const TYPE_DIALOG = 0;
    const TYPE_POLYLOGUE = 1;
    const TYPE_PROJECT = 2;

    public $assignUsers;
    public $assignClass;
    public $assignClassField;

    public function setAssignParams()
    {
        $this->assignClass = UserHasDialog::className();
        $this->assignClassField = 'dialog_id';
    }

    public function rules()
    {
        return [
            [['title'], 'string'],
            [['type', 'created_at', 'user_id', 'project_id'], 'integer'],
            [['assignUsers'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['title'] = Yii::t('dialog', 'Title');
        $labels['project_id'] = Yii::t('dialog', 'Project ID');

        return $labels;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {
            if ($identity = Yii::$app->user->identity) {
                $this->user_id = $identity->getId();
            }
        }

        return parent::beforeSave($insert);
    }

    public static function getTypes()
    {
        return [
            self::TYPE_DIALOG => Yii::t('dialog', 'Dialog'),
            self::TYPE_POLYLOGUE => Yii::t('dialog', 'Polylogue'),
            self::TYPE_PROJECT => Yii::t('dialog', 'Project dialog'),
        ];
    }

    public static function getType($type)
    {
        return self::getTypes()[$type];
    }

    public static function userDialog($userId)
    {
        /** @var User $user */
        $user = User::find()
            ->where(['id' => $userId])
            ->with(['dialogs'])
            ->one();

        return $user->dialogs;
    }

    public static function createProjectDialog(Project $project)
    {
        $dialog = new self();
        $dialog->type = self::TYPE_PROJECT;
        $dialog->title = $project->name;
        $dialog->project_id = $project->id;
        $dialog->save();
    }

    public static function getDialogForTwo(User $user, $userId)
    {
        /** @var User $destinationUser */
        $destinationUser = User::findOne($userId);
        $dialog = self::find()
            ->andWhere(['type' => self::TYPE_DIALOG])
            ->andWhere([
                'OR',
                ['title' => $user->username . ' - ' . $destinationUser->username],
                ['title' => $destinationUser->username . ' - ' . $user->username],
            ])->one();
        if (!$dialog) {
            $dialog = self::createDialogForTwo($user, $destinationUser);
            if ($dialog->hasErrors()) {
                return false;
            }
        }

        return $dialog;
    }

    public static function createDialogForTwo(User $user1, User $user2)
    {
        $dialog = new self();
        $dialog->type = self::TYPE_DIALOG;
        $dialog->title = $user1->username . ' - ' . $user2->username;
        $dialog->save();

        UserHasDialog::assignDialog($user1->id, $dialog->id);
        UserHasDialog::assignDialog($user2->id, $dialog->id);

        return $dialog;
    }
}