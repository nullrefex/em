<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\dialog\models;

use app\modules\project\models\Project;
use app\modules\user\interfaces\IAssign;
use nullref\dialog\models\UserHasDialog as BaseUserHasDialog;

class UserHasDialog extends BaseUserHasDialog implements IAssign
{
    public static function assignDialog($userId, $dialogId)
    {
        $isExist = self::findOne(['user_id' => $userId, 'dialog_id' => $dialogId]);

        if (!$isExist) {
            /** @var UserHasDialog $userDialog */
            $userDialog = new UserHasDialog();
            $userDialog->user_id = $userId;
            $userDialog->dialog_id = $dialogId;
            $userDialog->save();
        }
    }

    public static function assignToProjectDialog($userId, Project $project)
    {
        /** @var Dialog $dialog */
        $dialog = Dialog::findOne(['project_id' => $project->id]);

        if (!$dialog) {
            $dialog = Dialog::createProjectDialog($project);
        }

        self::assignDialog($userId, $dialog->id);
    }

    public static function getAssignUserIds($entityId)
    {
        return self::find()
            ->select(['user_id'])
            ->where(['dialog_id' => $entityId])
            ->column();
    }

    public static function getDialogUserIds($dialogId)
    {
        return self::find()
            ->select(['user_id'])
            ->where(['dialog_id' => $dialogId])
            ->column();
    }

    public static function getUserDialogIds($userId)
    {
        return self::find()
            ->select(['dialog_id'])
            ->where(['user_id' => $userId])
            ->column();
    }
}