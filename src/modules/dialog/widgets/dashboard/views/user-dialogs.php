<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2017 NRE
 *
 * @var $dialogs Dialog[]
 */
use app\modules\dialog\models\Dialog;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="col-lg-4">
    <div class="panel panel-default">
        <div class="panel-heading">
            <i class="fa fa-comments fa-fw"></i><?= Yii::t('dialog', 'Your chats') ?>
            <div class="btn-group pull-right">
            </div>
        </div>
        <div class="panel-body">
            <div class="list-group">
                <?php if (count($dialogs)): ?>
                    <?php foreach ($dialogs as $dialog): ?>
                        <a href="<?= Url::to(['/dialog/dialog/dialog', 'id' => $dialog->id,]) ?>"
                           target="_blank" class="list-group-item">
                            <i class="fa fa-comment fa-fw"></i>
                            <?= Html::encode($dialog->title) ?>
                            <?php if ($dialog->type): ?>
                                <span class="pull-right text-muted small">
                                    <em><?= Dialog::getType($dialog->type) ?></em>
                                </span>
                            <?php endif ?>
                        </a>
                    <?php endforeach ?>
                <?php else: ?>
                    <h3><?= Yii::t('project', 'You don\'t have any chats') ?></h3>
                <?php endif ?>
            </div>
        </div>
    </div>
</div>
