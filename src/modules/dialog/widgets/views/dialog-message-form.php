<?php
/**
 *
 * @var \nullref\dialog\models\MessageForm $messageForm
 * @var \nullref\dialog\widgets\Dialog $dialog
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

?>

<div class="dialog-message-form">
    <?php $form = ActiveForm::begin([
        'action' => [
            '/dialog/dialog/dialog',
            'id' => $dialog->id,
            'action' => 'create'
        ],
        'options' => [
            'data-method' => 'post',
            'data-pjax' => true,
        ]
    ]) ?>
    <?= $form->field($messageForm, 'userId')->hiddenInput()->label(false) ?>
    <?= $form->field($messageForm, 'text')->textarea()->label(Yii::t('dialog', 'Text')) ?>
    <?= Html::submitButton(Yii::t('dialog', 'Send'), ['class' => 'btn btn-primary']) ?>
    <?php ActiveForm::end() ?>
</div>
