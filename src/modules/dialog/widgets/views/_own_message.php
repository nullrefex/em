<?php
/**
 *
 * @var  $canDelete        boolean
 * @var \nullref\dialog\models\Message $model
 */
use rmrevin\yii\fontawesome\FA;
use yii\helpers\Html;
use yii\helpers\Url;

?>


<div class="message pull-right well">
    <p class="message-author">
        <?= Html::encode($model->getUser()->getDialogUsername()) ?>
    </p>
    <?php if ($canDelete): ?>
        <p class="delete-button">
            <?= Html::a(FA::i(FA::_TRASH), [
                '/dialog/dialog/dialog',
                'id' => $model->dialog_id,
                'messageId' => $model->id,
                'action' => 'delete'
            ], [
                'class' => 'btn btn-xs btn-danger pull-right remove-button',
                'data-method' => 'post',
                'data-pjax' => true,
            ]) ?>
        </p>
    <?php endif ?>
    <?= nl2br(Html::encode($model->text)) ?>
    <p class="create-at"><?= Yii::$app->formatter->asDatetime($model->created_at, 'short') ?></p>
</div>
<div class="clearfix"></div>