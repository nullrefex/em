<?php
/**
 * @author    Velychko Yaroslav
 */

namespace app\modules\dialog\widgets;


use nullref\dialog\models\Message;
use nullref\dialog\widgets\Messages as BaseMessages;
use Yii;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class Messages extends BaseMessages
{
    public function run()
    {
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $action = Yii::$app->request->get('action', false);
            if ($action == 'delete') {
                $messageId = Yii::$app->request->get('messageId', false);
                $this->removeMessage($messageId);
            }
        }

        $dataProvider = new ActiveDataProvider(ArrayHelper::merge([
            'query' => $this->dialog->getMessagesRelation()
                ->with(['userRelation'])
                ->orderBy(['id' => SORT_DESC]),
        ], $this->dataProviderConfig));

        return $this->render('messages', [
            'dataProvider' => $dataProvider,
            'user' => $this->user,
            'canDelete' => $this->canDelete,
        ]);
    }

    public function removeMessage($id)
    {
        /** @var Message $model */
        $model = Message::findOne($id);
        $model->delete();
    }

}