<?php
/**
 * @author    Dmytro Karpovych
 * @copyright 2016 NRE
 */


namespace app\modules\dialog\widgets;


use app\modules\dialog\models\Dialog;
use nullref\dialog\models\MessageForm;
use nullref\dialog\widgets\DialogMessageForm as BaseDialogMessageForm;
use Yii;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class DialogMessageForm extends BaseDialogMessageForm
{
    public function run()
    {
        /** @var MessageForm $messageForm */
        $messageForm = Yii::createObject(MessageForm::className());

        if ($this->user !== null) {
            $messageForm->userId = $this->user->getId();
        }

        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $action = Yii::$app->request->get('action', false);
            if ($action == 'create') {
                $dialogId = Yii::$app->request->get('id', false);
                if ($dialogId) {
                    $dialog = $this->findDialog($dialogId);
                    if ($dialog && $messageForm->load(Yii::$app->request->post())) {
                        $messageForm->createMessage($dialog);
                        $messageForm->text = '';
                    }
                }
            }
        }

        return $this->render('dialog-message-form', [
            'messageForm' => $messageForm,
            'dialog' => $this->dialog,
        ]);
    }


    /**
     * @param $dialogId
     * @return Dialog
     * @throws NotFoundHttpException
     */
    protected function findDialog($dialogId)
    {
        $model = Dialog::findOne($dialogId);

        if ($model === null) {
            throw  new NotFoundHttpException();
        }

        return $model;
    }
}