<?php
namespace app\modules\dialog;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;

class Bootstrap implements BootstrapInterface
{
    /**
     * Bootstrap method to be called during application bootstrap stage.
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        /** @var Module $module */
        if ((($module = $app->getModule('dialog')) == null) || !($module instanceof Module)) {
            return;
        };
    }
}
