<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\dialog\models\Dialog */

$this->title = Yii::t('dialog', 'Update {modelClass}: ', [
    'modelClass' => 'Dialog',
]) . ' ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('dialog', 'Dialogs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('dialog', 'Update');
?>
<div class="dialog-update">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('dialog', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
