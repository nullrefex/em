<?php

use app\modules\dialog\models\Dialog as DialogModel;
use app\modules\user\models\User;
use app\modules\dialog\widgets\DialogMessageForm;
use app\modules\dialog\widgets\Messages;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\widgets\Pjax;

/**
 * @var $this View
 * @var $user User
 * @var $dialog DialogModel
 */

$this->title = Yii::t('dialog', 'Dialog');
$this->params['breadcrumbs'][] = $this->title;
$currentUrl = Url::current();
$this->registerJs(<<<JS
    window.currentUrl = '$currentUrl';
    window.dialogId = '$dialog->id';
JS
);
$this->registerJsFile('/js/admin/dialog/main.js', ['depends' => JqueryAsset::className()]);

?>
<div class="dialog-page">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?php Pjax::begin([
        'id' => 'messageWidget',
        'enablePushState' => false,
        'enableReplaceState' => false,
        'timeout' => 99999,
    ]); ?>

    <?= Messages::widget([
        'dialog' => $dialog,
        'user' => $user,
        'dataProviderConfig' => [
            'pagination' => false,
        ],
        'canDelete' => true,
    ]) ?>

    <?php Pjax::end(); ?>

    <?php Pjax::begin([
        'id' => 'formWidget',
        'enablePushState' => false,
        'enableReplaceState' => false,
        'timeout' => 99999,
    ]); ?>

    <?= DialogMessageForm::widget(['dialog' => $dialog, 'user' => $user]) ?>

    <?php Pjax::end(); ?>

    <br>

</div>