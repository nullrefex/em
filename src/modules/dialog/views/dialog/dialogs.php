<?php

use app\modules\dialog\models\Dialog;
use app\modules\user\models\User;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $dialogs Dialog[]
 */

$this->title = Yii::t('dialog', 'Dialogs');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJs(<<<JS
    jQuery('select[name="user"]').change(function(){
        var value = jQuery(this).val();
        var link = jQuery('.write-link');
        var href = link.data('href');
        link.attr('href', href + '?userId=' + value);
    });
JS
);

?>
<div class="dialog-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6 col-sm-12">
            <h2><?= Yii::t('dialog', 'Your dialogs') ?></h2>
            <?php $i = 1;
            $type = '';
            foreach ($dialogs as $key => $dialog): ?>
                <?php
                if ($i % 4 == 0) {
                    $class = 'btn btn-success';
                } elseif ($i % 3 == 0) {
                    $class = 'btn btn-primary';
                } elseif ($i % 2 == 0) {
                    $class = 'btn btn-warning';
                } else {
                    $class = 'btn btn-danger';
                }
                $i++;
                ?>
                <?php if ($type !== $dialog->type) {
                    $type = $dialog->type;
                    echo '<h4>' . Dialog::getType($dialog->type) . '</h4>';
                } ?>
                <?= Html::a($dialog->title, ['dialog', 'id' => $dialog->id], [
                    'class' => $class,
                    'target' => '_blank',
                ]) ?>
            <?php endforeach; ?>
        </div>
        <div class="col-lg-6 col-sm-12">
            <h2><?= Yii::t('dialog', 'Write to...') ?></h2>

            <div class="row">
                <div class="col-lg-8 col-sm-8 col-xs-7">
                    <?= Select2::widget([
                        'name' => 'user',
                        'value' => '',
                        'data' => User::getMap('username', 'id', ['NOT', ['id' => Yii::$app->user->getId()]]),
                        'options' => [
                            'multiple' => false,
                            'placeholder' => Yii::t('dialog', 'Chose companion')
                        ]
                    ]); ?>
                </div>
                <div class="col-lg-4 col-sm-4 col-xs-3">
                    <?= Html::a(Yii::t('dialog', 'Write'), Url::to(['/dialog/dialog/write-to']), [
                        'class' => 'btn btn-primary write-link',
                        'target' => '_blank',
                        'data' => [
                            'href' => Url::to(['/dialog/dialog/write-to']),
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>