<?php

use app\modules\dialog\models\Dialog;
use app\modules\project\models\Project;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\dialog\models\Dialog */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="dialog-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput([
        'maxlength' => true,
        'readOnly' => ($model->type === Dialog::TYPE_DIALOG)
    ]) ?>

    <?= $form->field($model, 'type')->dropDownList(Dialog::getTypes()) ?>

    <?= $form->field($model, 'project_id')->dropDownList(Project::getMap(), [
        'prompt' => Yii::t('dialog', 'Choose project'),
        'readOnly' => ($model->type === Dialog::TYPE_PROJECT)
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('dialog', 'Create') : Yii::t('dialog', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
