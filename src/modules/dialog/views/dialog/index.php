<?php

use app\modules\dialog\models\Dialog;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $dataProvider ActiveDataProvider
 */

$this->title = Yii::t('dialog', 'Dialogs');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="dialog-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>


    <p>
        <?= Html::a(Yii::t('dialog', 'Create Dialog'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'title',
                [
                    'label' => Yii::t('dialog', 'User'),
                    'attribute' => 'user',
                    'value' => function (Dialog $model) {
                        return ($model->user) ? $model->user->username : '-';
                    },
                ],
                [
                    'label' => Yii::t('dialog', 'Type'),
                    'attribute' => 'type',
                    'value' => function (Dialog $model) {
                        return Dialog::getType($model->type);
                    },
                ],
                [
                    'label' => Yii::t('dialog', 'Project'),
                    'attribute' => 'project',
                    'value' => function (Dialog $model) {
                        return ($model->project) ? $model->project->name : '-';
                    },
                ],
                'created_at:date',

                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view} {update} {delete} {addUsers}',
                    'buttons' => [
                        'addUsers' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-user"></span>', ['assign-users',
                                'id' => $model->id,
                            ], [
                                'title' => Yii::t('user', 'Add users'),
                                'aria-label' => Yii::t('user', 'Add users'),
                                'data-method' => 'post',
                            ]);
                        }
                    ],
                ],
            ],
        ]); ?>
    </div>
</div>
