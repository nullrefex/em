<?php

use app\modules\tempo\models\Time;
use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\tempo\models\Time */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="time-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($model->user_id): ?>
        <div>
            <?= Html::label(Yii::t('user', 'User') . ': ' . $model->user->username) ?>
        </div>
    <?php else: ?>
        <?= $form->field($model, 'user_id')->textInput() ?>
    <?php endif ?>

    <?php if ($model->project_id): ?>
        <div>
            <?= Html::label(Yii::t('project', 'Project') . ': ' . $model->project->name) ?>
        </div>
    <?php else: ?>
        <?= $form->field($model, 'project_id')->textInput() ?>
    <?php endif ?>

    <?php if ($model->task_id): ?>
        <div>
            <?= Html::label(Yii::t('project', 'Task') . ': ' . $model->task->name) ?>
        </div>
    <?php else: ?>
        <?= $form->field($model, 'task_id')->textInput() ?>
    <?php endif ?>

    <?= $form->field($model, 'value')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'created_at')->widget(DatePicker::classname(), [
        'language' => Yii::$app->language,
        'dateFormat' => 'dd.MM.yyyy',
        'options' => [
            'class' => 'form-control',
        ],
        'clientOptions' => ['defaultDate' => date(Time::DATE_FORMAT)],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('tempo', 'Create') : Yii::t('tempo', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
