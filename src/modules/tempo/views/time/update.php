<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\tempo\models\Time */

$this->title = Yii::t('tempo', 'Update {modelClass}: ', [
    'modelClass' => 'Time',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('tempo', 'Times'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('tempo', 'Update');
?>
<div class="time-update">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('tempo', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
