<?php

use app\modules\project\models\Project;
use app\modules\user\models\User;
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\tempo\models\TimeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('tempo', 'Times');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-index">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('tempo', 'Create Time'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'user' => [
                'attribute' => 'user_id',
                'filter' => User::getMap('username'),
                'format' => 'user',
                'value' => 'user_id',
            ],
            'project' => [
                'attribute' => 'project_id',
                'filter' => Project::getMap(),
                'format' => 'projectLink',
                'value' => 'project_id',
            ],
            'task' => [
                'attribute' => 'task_id',
                'format' => 'taskLink',
                'value' => 'task_id',
            ],
            'value',
            'created_at:date',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
