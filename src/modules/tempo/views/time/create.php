<?php

use yii\helpers\Html;
use yii\jui\DatePicker;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\modules\tempo\models\Time */

$this->title = Yii::t('tempo', 'Create Time');
$this->params['breadcrumbs'][] = ['label' => Yii::t('tempo', 'Times'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-create">

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                <?= Html::encode($this->title) ?>
            </h1>
        </div>
    </div>

    <p>
        <?= Html::a(Yii::t('tempo', 'List'), ['index'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>
