<?php
/**
 * @var $this \yii\web\View
 */
use yii\helpers\Html;

$this->title = Yii::t('tempo','Time tracking');
?>


<div class="tempo-index">
    <div class="project-view">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?= Html::encode($this->title) ?>
                </h1>
            </div>
        </div>
    </div>
</div>
