<?php

namespace app\modules\tempo\models;

use app\modules\project\models\Project;
use app\modules\project\models\Task;
use app\modules\user\models\User;
use nullref\useful\DateBehavior;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%tempo_time}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $project_id
 * @property int $task_id
 * @property string $value
 * @property int $created_at
 *
 *
 * @property User $user
 * @property Project $project
 * @property Task $task
 */
class Time extends ActiveRecord
{
    const DATE_FORMAT = 'd.m.Y';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%tempo_time}}';
    }

    /**
     * @inheritdoc
     * @return TimeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TimeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'project_id', 'task_id'], 'integer'],
            [['value'], 'number'],
            [['created_at', 'value', 'user_id', 'project_id', 'task_id'], 'required'],
            [['created_at'], 'default', 'value' => date(self::DATE_FORMAT)],
        ];
    }

    public function behaviors()
    {
        return [
            'time' => [
                'class' => DateBehavior::className(),
                'fields' => ['created_at'],
                'format' => self::DATE_FORMAT,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tempo', 'ID'),
            'user_id' => Yii::t('tempo', 'User'),
            'project_id' => Yii::t('tempo', 'Project'),
            'task_id' => Yii::t('tempo', 'Task'),
            'value' => Yii::t('tempo', 'Time Amount'),
            'created_at' => Yii::t('tempo', 'Added'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
}
