<?php

namespace app\modules\tempo;

use nullref\core\interfaces\IAdminModule;
use Yii;
use yii\base\Module as BaseModule;

/**
 * tempo module definition class
 */
class Module extends BaseModule implements IAdminModule
{
    public $controllerAliases = [
        '@app/modules/tempo/controllers',
    ];

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\tempo\controllers';

    /**
     * @return array
     */
    public static function getAdminMenu()
    {
        return [
            'icon' => 'clock-o',
            'label' => Yii::t('tempo', 'Time tracking'),
            'url' => ['/tempo/time'],
            'order' => 5,
            'roles' => ['project-manager', 'developer'],
        ];
    }
}
