<?php
/**
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $columns array
 * @var $addUrl mixed
 */
use yii\grid\GridView;
use yii\helpers\Html;

?>

<?php if ($addUrl): ?>
    <p>
        <?= Html::a(Yii::t('tempo', 'Add record'), $addUrl, ['class' => 'btn btn-primary']) ?>
    </p>
<?php endif ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => $columns,
]) ?>
