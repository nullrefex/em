<?php

namespace app\modules\tempo\widgets;

use app\modules\tempo\models\Time;
use Yii;
use yii\base\Widget;
use yii\data\ActiveDataProvider;

class Table extends Widget
{
    public $taskId = null;

    public $projectId = null;

    public $userId = null;

    public $addUrl = [];

    protected $dataProvider;

    protected $columns = [];


    /**
     *
     */
    public function init()
    {
        $query = Time::find();
        $columns = [
            'num' => ['class' => 'yii\grid\SerialColumn'],
            'user' => [
                'label' => Yii::t('user', 'User'),
                'format' => 'user',
                'value' => 'user_id',
            ],
            'project' => [
                'label' => Yii::t('project', 'Project'),
                'format' => 'projectLink',
                'value' => 'project_id',
            ],
            'task' => [
                'label' => Yii::t('project', 'Task'),
                'format' => 'taskLink',
                'value' => 'task_id',
            ],
            'value' => [
                'label' => Yii::t('tempo', 'Time Amount'),
                'value' => 'value',
            ],
            'createdAt' => [
                'label' => Yii::t('tempo', 'Added'),
                'format' => 'date',
                'value' => 'created_at',
            ],
        ];

        if ($this->userId) {
            $query->andWhere(['user_id' => $this->userId]);
            unset($columns['user']);
        }
        if ($this->taskId) {
            $query->andWhere(['task_id' => $this->taskId]);
            unset($columns['task']);
            unset($columns['project']);
        }
        if ($this->projectId) {
            $query->andWhere(['project_id' => $this->projectId]);
            unset($columns['project']);
        }

        if (($this->addUrl == null) && $this->userId && $this->taskId) {
            $this->addUrl = ['/tempo/time/create', 'user_id' => $this->userId, 'task_id' => $this->taskId];
        }

        $this->dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $this->columns = array_values($columns);
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('table', [
            'dataProvider' => $this->dataProvider,
            'columns' => $this->columns,
            'addUrl' => $this->addUrl,
        ]);
    }
}