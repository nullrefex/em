<?php

namespace app\modules\tempo\migrations;

use yii\db\Migration;

class M170508092542Add_tempo_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%tempo_time}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'project_id' => $this->integer(),
            'task_id' => $this->integer(),
            'value' => $this->decimal(10, 2),
            'created_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%tempo_time}}');
    }
}
