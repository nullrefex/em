<?php

namespace app\modules\tempo\controllers;

use nullref\core\interfaces\IAdminController;
use yii\web\Controller;

/**
 * Default controller for the `tempo` module
 */
class DefaultController extends Controller implements IAdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
