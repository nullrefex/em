<?php

namespace app\modules\user\migrations;

use yii\db\Migration;

class M170514125342Update_user_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%user}}', 'registration_ip', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%user}}', 'registration_ip', $this->bigInteger());

        return true;
    }
}
