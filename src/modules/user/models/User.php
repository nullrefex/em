<?php

namespace app\modules\user\models;

use app\modules\dialog\models\Dialog;
use app\modules\dialog\models\UserHasDialog;
use app\modules\rbac\models\AuthAssignment;
use app\modules\project\traits\ProjectUserTrait;
use nullref\dialog\interfaces\UserModel;
use nullref\fulladmin\modules\user\models\User as BaseUser;
use nullref\useful\traits\Mappable;
use rmrevin\yii\module\Comments\interfaces\CommentatorInterface;
use Yii;
use yii\db\Query;

/**
 * User model
 *
 * @property string roles
 * @property Dialog[] $dialogs
 * @property Dialog[] $allDialogs
 *
 */
class User extends BaseUser implements UserModel, CommentatorInterface
{
    use Mappable;
    use ProjectUserTrait;

    /**
     * @return bool Whether the user is an admin or not.
     */
    public function getIsAdmin()
    {
        return parent::getIsAdmin() || $this->getAttribute('is_admin');
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        $roles = AuthAssignment::find()
            ->select(['item_name'])
            ->where(['user_id' => $this->id])
            ->asArray()
            ->column();

        $result = array_reduce($roles, function ($list, $role) {
            $list = array_merge(User::getItems($role), $list, [$role]);
            return $list;
        }, []);

        return $result;
    }

    /**
     * @param $item
     * @return array|mixed
     */
    public static function getItems($item)
    {
        $children = Yii::$app->db->cache(function () use ($item) {
            $query = new Query();
            return $query->select(['child'])
                ->from(Yii::$app->authManager->itemChildTable)
                ->where(['parent' => $item])
                ->column();
        });

        foreach ($children as $parent) {
            $children = array_merge(self::getItems($parent), $children);
        }
        return $children;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDialogs()
    {
        return $this->hasMany(Dialog::className(), ['id' => 'dialog_id'])
            ->viaTable(UserHasDialog::tableName(), ['user_id' => 'id']);
    }

    /**
     * @return array|\nullref\dialog\models\Dialog[]
     */
    public function getAllDialogs()
    {
        $dialogs = Dialog::find()
            ->where(['id' => UserHasDialog::find()
                ->select(['dialog_id'])
                ->where(['user_id' => $this->id])
                ->column()
            ])->orderBy(['type' => SORT_DESC])
            ->all();

        return $dialogs;
    }


    /**
     * @return string
     */
    public function getDialogUsername()
    {
        return $this->username;
    }

    /**
     * @return null|string
     */
    public function getCommentatorAvatar()
    {
        return $this->profile->getAvatarUrl();
    }

    /**
     * @return string
     */
    public function getCommentatorName()
    {
        return $this->username;
    }

    /**
     * @return array
     */
    public function getCommentatorUrl()
    {
        return ['/user/profile', 'id' => $this->id];
    }
}