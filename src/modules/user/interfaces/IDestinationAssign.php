<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\interfaces;


interface IDestinationAssign
{
    public function setAssignParams();

}