<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\interfaces;


interface IAssign
{
    public static function getAssignUserIds($entityId);
}