<?php

use app\modules\user\models\User;
use kartik\select2\Select2;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $isUpdated boolean
 * @var $model \yii\base\Model
 */

?>

<div class="row">
    <div class="col-lg-4">
        <?php if ($isUpdated): ?>

            <?= Alert::widget([
                'options' => [
                    'class' => 'alert-success'
                ],
                'body' => Yii::t('user', 'Users have been updated'),
            ]) ?>

        <?php endif ?>

        <?php $form = ActiveForm::begin([
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
        ]) ?>

        <?= Html::activeHiddenInput($model, 'id') ?>

        <?= $form->field($model, 'assignUsers')->widget(Select2::className(), [
            'data' => User::getMap('username'),
            'options' => [
                'id' => 'users',
                'multiple' => true
            ],
        ])->label(Yii::t('user', 'Users')) ?>

        <?= Html::submitButton(Yii::t('user', 'Update users'), ['class' => 'btn btn-success btn-block']) ?>

        <?php ActiveForm::end() ?>
    </div>
</div>