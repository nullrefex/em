<?php
namespace app\modules\user\widgets;

use app\modules\dialog\models\Dialog;
use app\modules\dialog\models\UserHasDialog;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class AssignUsers extends Widget
{
    public $entityId;
    public $assignClass;
    public $destinationClass;


    /** @inheritdoc */
    public function init()
    {
        parent::init();
        if ($this->entityId === null) {
            throw new InvalidConfigException('You should set ' . __CLASS__ . '::$entityId');
        }
        if ($this->assignClass === null) {
            throw new InvalidConfigException('You should set ' . __CLASS__ . '::$assignClass');
        }
        if ($this->destinationClass === null) {
            throw new InvalidConfigException('You should set ' . __CLASS__ . '::$destinationClass');
        }
    }

    /** @inheritdoc */
    public function run()
    {
        $isUpdated = false;

        $destinationObject = Yii::createObject($this->destinationClass);
        $model = $destinationObject::findOne($this->entityId);
        $assignObject = Yii::createObject($this->assignClass);
        $model->assignUsers = $assignObject::getAssignUserIds($this->entityId);

        if ($model->load(Yii::$app->request->post())) {
            $model->assignUsers();
            $isUpdated = true;
        }

        return $this->render('assign-users', [
            'model' => $model,
            'isUpdated' => $isUpdated,
        ]);
    }
}