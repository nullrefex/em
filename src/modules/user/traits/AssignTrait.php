<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\traits;


use Yii;
use yii\db\ActiveRecord;

trait AssignTrait
{
    public function assignUsers()
    {
        $this->setAssignParams();

        $model = $this->assignClass;
        $object = Yii::createObject($model);
        $field = $this->assignClassField;

        if (!is_array($this->assignUsers)) {
            $this->assignUsers = [];
        }

        $oldUsers = $object::getAssignUserIds($this->id);

        //Add new user
        $usersDiff = array_diff($this->assignUsers, $oldUsers);
        foreach ($usersDiff as $userId) {
            /** @var ActiveRecord $newUser */
            $newUser = new $object([
                'user_id' => $userId,
                $field => $this->id,
            ]);
            $newUser->save();
        }

        //Remove users
        $usersToRemove = [];
        foreach (array_diff($oldUsers, $this->assignUsers) as $userId) {
            $usersToRemove[] = $userId;
        }
        $object::deleteAll(['user_id' => $usersToRemove, $field => $this->id]);

        return true;
    }
}