<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\controllers;

use app\modules\rbac\filters\AccessControl;
use dektrium\user\controllers\AdminController as BaseAdminController;
use yii\filters\VerbFilter;

class AdminController extends BaseAdminController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}