<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\controllers;

use app\modules\rbac\filters\AccessControl;
use dektrium\user\controllers\SecurityController as BaseSecurityController;
use yii\filters\VerbFilter;

class SecurityController extends BaseSecurityController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login', 'auth'],
                        'roles' => ['?'],
                    ],
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionLogin()
    {
        $this->layout = '@app/modules/admin/views/layouts/base.php';
        return parent::actionLogin();
    }
}