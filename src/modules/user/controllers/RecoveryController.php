<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\controllers;

use app\modules\rbac\filters\AccessControl;
use dektrium\user\controllers\RecoveryController as BaseRecoveryController;
use yii\filters\VerbFilter;

class RecoveryController extends BaseRecoveryController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}