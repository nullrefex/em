<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\modules\user\controllers;

use app\modules\rbac\filters\AccessControl;
use dektrium\user\controllers\SettingsController as BaseSettingsController;
use yii\filters\VerbFilter;

class SettingsController extends BaseSettingsController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'controller' => $this,
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
}