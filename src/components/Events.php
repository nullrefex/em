<?php
/**
 * @author    Yaroslav Velychko
 */

namespace app\components;

use app\modules\dialog\models\Dialog;
use app\modules\dialog\models\UserHasDialog;
use app\modules\project\models\Project;
use app\modules\project\models\ProjectHasUser;
use app\modules\project\models\ProjectTaskHasUser;
use app\modules\rbac\models\AuthAssignment;
use app\modules\settings\models\GeneralSettings;
use app\modules\user\models\User;
use dektrium\user\models\Account;
use dektrium\user\models\Profile;
use rmrevin\yii\module\Comments\models\Comment;
use yii\base\BootstrapInterface;
use yii\base\Event;

class Events implements BootstrapInterface
{
    public function bootstrap($app)
    {
        Event::on(User::className(), User::AFTER_CONFIRM, [$this, 'assignMainDialog']);

        Event::on(User::className(), User::EVENT_BEFORE_DELETE, [$this, 'deleteUserRelations']);

        Event::on(Project::className(), Project::EVENT_AFTER_INSERT, [$this, 'createDialog']);

        Event::on(ProjectHasUser::className(), Project::EVENT_AFTER_INSERT, [$this, 'assignUserToDialog']);
    }

    public function assignMainDialog(Event $event)
    {
        /** @var User $user */
        $user = $event->sender;
        /** @var GeneralSettings $statusSettings */
        $generalSetting = new GeneralSettings();

        UserHasDialog::assignDialog($user->id, $generalSetting->appDialog);
    }

    public function deleteUserRelations(Event $event)
    {
        /** @var User $user */
        $user = $event->sender;
        $userId = $user->id;

        AuthAssignment::deleteAll(['user_id' => $userId]);
        Comment::deleteAll(['user_id' => $userId]);
        Dialog::deleteAll(['user_id' => $userId]);
        Dialog::updateAll(['user_id' => ''], ['user_id' => $userId]);
        Profile::deleteAll(['user_id' => $userId]);
        ProjectHasUser::deleteAll(['user_id' => $userId]);
        ProjectTaskHasUser::deleteAll(['user_id' => $userId]);
        Account::deleteAll(['user_id' => $userId]);
        UserHasDialog::deleteAll(['user_id' => $userId]);
    }

    public function createDialog(Event $event)
    {
        /** @var Project $project */
        $project = $event->sender;

        Dialog::createProjectDialog($project);
    }

    public function assignUserToDialog(Event $event)
    {
        /** @var ProjectHasUser $projectUser */
        $projectUser = $event->sender;
        /** @var Project $project */
        $project = ProjectHasUser::getCurrentProject($projectUser);

        UserHasDialog::assignToProjectDialog($projectUser->user_id, $project);
    }
}