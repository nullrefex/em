<?php
/**
 */

namespace app\components;


use app\modules\project\models\Project;
use app\modules\project\models\Status;
use app\modules\project\models\Task;
use app\modules\user\models\User;
use Yii;
use yii\helpers\Html;

class Formatter extends \yii\i18n\Formatter
{
    public function asUser($id)
    {
        return User::findOne(['id' => $id])->username;
    }

    public function asProjectStatus($id)
    {
        return Status::findOne(['id' => $id])->name;
    }

    public function asProjectLink($id)
    {
        $model = Project::findOne(['id' => $id]);
        return $model ? Html::a($model->name, ['/project/project/view', 'id' => $model->id]) : Yii::t('project', 'N\A');
    }

    public function asTaskLink($id)
    {
        $model = Task::findOne(['id' => $id]);
        return $model ? Html::a($model->name, ['/project/task/view', 'id' => $model->id]) : Yii::t('project', 'N\A');
    }
}