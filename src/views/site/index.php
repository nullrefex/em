<?php
use app\modules\dialog\widgets\dashboard\UserDialogs;
use app\modules\project\widgets\dashboard\Tasks;
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = Yii::$app->name;
?>
<div class="row">
    <div class="col-lg-12">
        <h1 class="page-header">
            <?= Html::encode($this->title) ?>
        </h1>
    </div>
</div>
<div class="row">
    <?= Tasks::widget() ?>
    <?= UserDialogs::widget() ?>
</div>