<?php

namespace tests;

use app\modules\project\models\Project;

class TaskTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testValidateProject()
    {
        $model = new Project();

        expect_not($model->validate());

        $model->name = 'Test project';

        $model->code = 'TP';

        expect($model->validate());
    }
}