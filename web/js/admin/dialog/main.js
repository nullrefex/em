/**
 * @author    Yaroslav Velychko
 */
jQuery(function () {
    var body = jQuery('body');

    //Scroll to bottom
    var scrollToBottom = function () {
        setTimeout(function () {
            var messagesWrapper = body.find('.dialog-messages');
            messagesWrapper.scrollTop(messagesWrapper[0].scrollHeight);
        }, 200);
    };
    //scrollToBottom();

    setInterval(function () {
        var lastId = jQuery('.dialog-messages div[data-key]:last').data('key');
        jQuery.ajax({
            url: '/dialog/dialog/check-message'.addUrlParam('id', window.dialogId).addUrlParam('lastId', lastId)
        }).done(function (isChanged) {
            if (isChanged) {
                jQuery.pjax.reload({container: '#messageWidget'});
            }
        });
    }, 5000);

    jQuery(document).on('pjax:complete', '#messageWidget', function (e) {
        //scrollToBottom();
    });

});